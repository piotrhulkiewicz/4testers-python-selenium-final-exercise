from selenium.webdriver.common.by import By


class ProjectsPage:

    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        add_project_button = self.browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT')
        add_project_button.click()

    def search_project_by_name(self, name):
        input_project_search = self.browser.find_element(By.CSS_SELECTOR, '#search')
        search_button = self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
        input_project_search.send_keys(name)
        search_button.click()
