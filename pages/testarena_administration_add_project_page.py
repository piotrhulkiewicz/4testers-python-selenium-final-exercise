from selenium.webdriver.common.by import By
from utils.utils_methods import MyUtils
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class AddProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def add_project(self):
        input_project_name = self.browser.find_element(By.CSS_SELECTOR, '#name')
        input_project_prefix = self.browser.find_element(By.CSS_SELECTOR, '#prefix')
        input_project_describe = self.browser.find_element(By.CSS_SELECTOR, '#description')
        save_project_button = self.browser.find_element(By.CSS_SELECTOR, '#save')
        project_name = MyUtils.get_random_string(10)
        input_project_name.send_keys(project_name)
        input_project_prefix.send_keys(MyUtils.get_random_string(6))
        input_project_describe.send_keys(MyUtils.get_random_string(20))
        save_project_button.click()
        return project_name


    def wait_until_add_project_message_is_visible_and_close(self,browser):
        wait = WebDriverWait(browser, 10)
        add_project_message_selector = (By.CSS_SELECTOR, '#j_info_box > div.j_close_button')
        add_project_message_close = wait.until(EC.element_to_be_clickable(add_project_message_selector))
        add_project_message_close.click()

    def back_to_the_projects_list(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.activeMenu').click()
