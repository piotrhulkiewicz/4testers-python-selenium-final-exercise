from selenium.webdriver.common.by import By


class CockpitPage:

    def __init__(self, browser):
        self.browser = browser

    def click_administration(self):
        administration_button = self.browser.find_element(By.CSS_SELECTOR, '.icon_tools')
        administration_button.click()
