from selenium.webdriver.common.by import By


class LoginPage:
    url = "http://demo.testarena.pl/zaloguj"
    login = "administrator@testarena.pl"
    password = "sumXQQ72$L"

    def __init__(self, browser):
        self.browser = browser

    def load_page(self, ):
        self.browser.get(self.url)

    def login_testarena(self, login, password):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(login)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, '#login').click()
