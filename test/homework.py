import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.testarena_login_page import LoginPage
from pages.testarena_cockpit_page import CockpitPage
from pages.testarena_administration_projects_page import ProjectsPage
from pages.testarena_administration_add_project_page import AddProjectPage


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load_page()
    login_page.login_testarena(LoginPage.login, LoginPage.password)
    yield browser
    browser.quit()


def test_add_and_search_project_on_testarena(browser):
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    project_page = ProjectsPage(browser)
    project_page.click_add_project()
    add_project_page = AddProjectPage(browser)
    project_name = add_project_page.add_project()
    add_project_page.wait_until_add_project_message_is_visible_and_close(browser)
    add_project_page.back_to_the_projects_list()
    project_page.search_project_by_name(project_name)
    assert browser.find_element(By.LINK_TEXT, project_name).is_displayed()
